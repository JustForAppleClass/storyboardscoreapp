//
//  ViewController.h
//  StoryBoardScoreApp
//
//  Created by Michelle Griffin on 6/24/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *PlayerOneName;

@property (weak, nonatomic) IBOutlet UITextField *PlayerTwoName;

//Labels
@property (weak, nonatomic) IBOutlet UILabel *PlayerOneScore;


@property (weak, nonatomic) IBOutlet UILabel *PlayerTwoScore;
//Stepper Properties
@property (weak, nonatomic) IBOutlet UIStepper *PlayerOneScoreKeepr;

@property (weak, nonatomic) IBOutlet UIStepper *PlayerTwoScoreKeeper;

//Stepper pressed action
- (IBAction)PlayerOneStepper:(id)sender;

- (IBAction)PlayerTwoStepper:(id)sender;
- (IBAction)ResetButton:(id)sender;

@end

