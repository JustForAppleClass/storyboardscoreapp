//
//  AppDelegate.h
//  StoryBoardScoreApp
//
//  Created by Michelle Griffin on 6/24/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

