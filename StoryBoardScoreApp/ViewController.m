//
//  ViewController.m
//  StoryBoardScoreApp
//
//  Created by Michelle Griffin on 6/24/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor blackColor];
    
    self.PlayerOneScore.text = @"0";
    self.PlayerTwoScore.text = @"0";
    self.PlayerTwoName.inputAccessoryView = [self GimmeFinishButton];
    self.PlayerOneName.inputAccessoryView = [self GimmeFinishButton];
    
    
}

-(UIView*)GimmeFinishButton {
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIButton* btn = [ UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"Finished" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(self.view.frame.size.width - 100, 0, 100, 50);
    [view addSubview:btn];
    
    return view;
}

-(void)done:(id)sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)PlayerTwoStepper:(id)sender {
    NSString *score = [NSString stringWithFormat:@"%d", (int)self.PlayerTwoScoreKeeper.value];
    
    self.PlayerTwoScore.text = score;
}

- (IBAction)ResetButton:(id)sender {
    self.PlayerTwoScoreKeeper.value = 0;
    self.PlayerOneScoreKeepr.value = 0;
    self.PlayerOneScore.text = @"0";
    self.PlayerTwoScore.text = @"0";
    
}
- (IBAction)PlayerOneStepper:(id)sender {
    
    NSString *score = [NSString stringWithFormat:@"%d", (int)self.PlayerOneScoreKeepr.value];
    
    self.PlayerOneScore.text = score;
}
@end
